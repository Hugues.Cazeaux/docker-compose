#!/bin/bash
uid=5000
gid=5000

if [ -z ${DEMO_DOMAIN_NAME} ];
then
    echo "DEMO_DOMAIN_NAME is unset";
    exit -1;
else
    echo "DEMO_DOMAIN_NAME is set to '$DEMO_DOMAIN_NAME'";
fi

# Create volumes for storage
createStorage(){
    local_num=$1
    local_storage_normal=dlcm-storagion$local_num-normal-storage-volume
    docker volume create $local_storage_normal
    docker run --rm -v $local_storage_normal:/data busybox sh -c "chown -R $uid:$gid /data"

    local_storage_secured=dlcm-storagion$local_num-secured-storage-volume
    docker volume create $local_storage_secured
    docker run --rm -v $local_storage_secured:/data busybox sh -c "chown -R $uid:$gid /data"
}

echo "Update configuration files:"
sed -i "s/%DEMO_DOMAIN_NAME%/dlcm.${DEMO_DOMAIN_NAME}/g" ./portal_config/environment.runtime.json

echo "Create docker volume for home:"
dlcm_home=dlcm-home-volume
docker volume create $dlcm_home
docker run --rm -v $dlcm_home:/data busybox sh -c "chown -R $uid:$gid /data"

echo "Create docker volume for storagion1:"
createStorage 1

echo "Create docker volume for storagion2:"
createStorage 2

echo "Create docker volume for portal:"
dlcm_portal=dlcm-portal-volume
docker volume create $dlcm_portal

docker create -v $dlcm_portal:/data --name portal busybox
docker cp portal_config/. portal:data
docker rm portal
docker run --rm -v $dlcm_portal:/data busybox sh -c "chown -R 101:101 /data"
