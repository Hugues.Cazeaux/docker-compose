#!/bin/bash

DLCM_VERSION=2.2.1
PWD=`pwd`

# Check if TOKEN is not set
if [[ -z "$TOKEN" ]]; then
    read -sp 'Token?' token
    echo
    export TOKEN=$token
fi

docker run --network="host" --env=TOKEN --mount="type=bind,source=${PWD}/properties/dlcm-demo-data.properties,target=/dlcm-it-tests.properties,readonly" --rm -it dlcm-demo-data:$DLCM_VERSION