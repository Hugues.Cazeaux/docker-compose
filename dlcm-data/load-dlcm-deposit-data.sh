#!/bin/bash

DLCM_VERSION=2.2.1
PWD=`pwd`
export IT_OPTIONS=-Ddlcm.demo-data.enabled=true

# Check if TOKEN is not set
if [[ -z "$TOKEN" ]]; then
    read -sp 'Token?' token
    echo
    export TOKEN=$token
fi

# Test to run
if [[ -z "$IT_TESTS" ]]; then
    echo
    echo "DepositsDemoIT => To create deposits and archive them"
    echo "MetadataEditionDepositsDemoIT => To edit existing archives and submit them"
    echo "CollectionsDemoIT => To create archive collections and archive them"
    echo "CollectionsOfCollectionsDemoIT => To create archive collections of collections and archive them"
    read -p 'Test to run?' testtorun
    echo
    export IT_TESTS=testtorun
fi

 docker run --network="host" --env=TOKEN --env=IT_TESTS --env=IT_OPTIONS  --mount="type=bind,source=${PWD}/properties/dlcm-demo-data.properties,target=/dlcm-it-tests.properties,readonly" --rm -it dlcm-demo-data:$DLCM_VERSION