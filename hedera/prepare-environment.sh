#!/bin/bash
uid=5000
gid=5000

if [ -z ${DEMO_DOMAIN_NAME} ];
then
    echo "DEMO_DOMAIN_NAME is unset";
    exit -1;
else
    echo "DEMO_DOMAIN_NAME is set to '$DEMO_DOMAIN_NAME'";
fi


echo "Update configuration files:"
sed -i "s/%DEMO_DOMAIN_NAME%/hedera.${DEMO_DOMAIN_NAME}/g" ./portal_config/environment.runtime.json

echo "Create docker volume for home:"
hedera_home=hedera-home-volume
docker volume create $hedera_home
docker run --rm -v $hedera_home:/data busybox sh -c "chown -R $uid:$gid /data"

echo "Create docker volume for storage:"
hedera_storage=hedera-storage-volume
docker volume create $hedera_storage
docker run --rm -v $hedera_storage:/data busybox sh -c "chown -R $uid:$gid /data"

echo "Create docker volume for portal:"
hedera_portal=hedera-portal-volume
docker volume create $hedera_portal

docker create -v $hedera_portal:/data --name portal busybox
docker cp portal_config/. portal:data
docker rm portal
docker run --rm -v $hedera_portal:/data busybox sh -c "chown -R 101:101 /data"
